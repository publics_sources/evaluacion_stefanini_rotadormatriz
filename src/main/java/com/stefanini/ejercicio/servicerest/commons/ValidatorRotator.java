package com.stefanini.ejercicio.servicerest.commons;

@FunctionalInterface
public interface ValidatorRotator {

	boolean validate(Integer[][] matriz);
}
