package com.stefanini.ejercicio.servicerest.commons;

@FunctionalInterface
public interface RotatorUtils {

	Integer getNewValueRotate(Integer[][] matriz, int x, int y);
}
