package com.stefanini.ejercicio.servicerest.dtos;

public class ObjectResponse<T> {
	
	private static final String MSG_OK_DEFAULT = "Operación ejecutada correctamente";
	private static final String MSG_ERROR_DEFAULT = "Hubo un error inesperado";
	private static final String STATUS_OK = "OK";
	private static final String STATUS_ERROR = "ERROR";

	private T result;
	private String message;
	private String status;
	
	public void setOk() {
		this.status = STATUS_OK;
		this.message = MSG_OK_DEFAULT;
	}
	
	public void setOk(String message) {
		this.status = STATUS_OK;
		this.message = message;
	}
	
	public void setError() {
		this.status = STATUS_ERROR;
		this.message = MSG_ERROR_DEFAULT;
	}
	
	public void setError(String message) {
		this.status = STATUS_ERROR;
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
