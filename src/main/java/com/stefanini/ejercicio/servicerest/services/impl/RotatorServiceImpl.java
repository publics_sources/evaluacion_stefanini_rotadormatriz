package com.stefanini.ejercicio.servicerest.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.stefanini.ejercicio.servicerest.commons.RotatorUtils;
import com.stefanini.ejercicio.servicerest.commons.ValidatorRotator;
import com.stefanini.ejercicio.servicerest.services.RotatorService;

@Service
public class RotatorServiceImpl implements RotatorService {
	
	private static final Logger logger = LogManager.getLogger(RotatorServiceImpl.class);

	@Override
	public Integer[][] rotate(Integer[][] matrizIn) throws Exception {
		try {
			ValidatorRotator validator = (matriz)-> {
				if (matriz == null || matriz[0] == null) return false;
				
				int n = matriz.length;
				int m = matriz[0].length;
				
				if (n != m) return false;
				
				for (Integer[] mat : matriz) {
					if (mat.length != n) return false;
					for (Integer val : mat) {
						if (val == null) return false;
					}
				}
				
				return n == m;
			};
			
			if (!validator.validate(matrizIn)) {
				throw new Exception("Matriz no válida");
			}
			
			logger.info("Validate successfull!!");
			
			RotatorUtils rotator = (m, x, y) -> {
				int n = m.length;
				int oldX = y;
				int oldY = n - 1 - x;
				return m[oldX][oldY];
			};
			
			
			int size = matrizIn.length;
			Integer[][] matrizOut = new Integer[size][size];
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					matrizOut[i][j] = rotator.getNewValueRotate(matrizIn, i, j);
				}
			}
			
			return matrizOut;
			
		} catch (Exception e) {
			throw e;
		}
	}

}
