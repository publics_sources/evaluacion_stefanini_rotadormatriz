package com.stefanini.ejercicio.servicerest.services;

import org.springframework.stereotype.Service;

@Service
public interface RotatorService {
	
	Integer[][] rotate(Integer[][] matrizIn) throws Exception;
}
