package com.stefanini.ejercicio.servicerest.resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stefanini.ejercicio.servicerest.dtos.ObjectResponse;
import com.stefanini.ejercicio.servicerest.services.RotatorService;

@RestController
@RequestMapping("/api/rotator")
public class RotatorResource {
	
	private static final Logger logger = LogManager.getLogger(RotatorResource.class);
	
	@Autowired
	private RotatorService rotatorSrv;


	
	@PostMapping
	public ResponseEntity<ObjectResponse<Integer[][]>> rotate(@RequestBody Integer[][] matrizIn) {
		ObjectResponse<Integer[][]> response = new ObjectResponse<>();
		try {
			Integer[][] matrizOut = this.rotatorSrv.rotate(matrizIn);
			response.setResult(matrizOut);
			response.setOk();
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error("error : " + e.getMessage());
			response.setError(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
